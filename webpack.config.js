const PATH = require('path'),
NAME = 'app', // バンドルファイル名
ENV = 0; // 1(trury) production 0(falsey) development
MINI_CSS_EXTACT_PLUGIN = require('mini-css-extract-plugin'), //webpack4 CSS別ファイル出力
OPTIMIZE_CSS_ASSETS_PLUGIN = require('optimize-css-assets-webpack-plugin'), //mini-css-extract-pluginで別ファイルに吐き出したCSSをミニファイ化
PRETTIER_WEBPACK_PLUGIN = require('prettier-webpack-plugin');


let plugins = [ // プラグイン設定
 new MINI_CSS_EXTACT_PLUGIN({filename:'../css/[name].css'}), //CSSファイル別ファイル吐き出し
 new PRETTIER_WEBPACK_PLUGIN({ //prettier SCSS,JS (HTMLは対応プラグイン自体対応外のためprettier本体で対応)
  widthPrint: 100000,
  tabWidth:1,
  singleQuote: true
 })
];

if(ENV) plugins.push(new OPTIMIZE_CSS_ASSETS_PLUGIN()); //CSSファイルミニファイ化

module.exports = {
 mode: ENV ? 'production' : 'development',
 entry: {
  [NAME]:'./_dev/js/index.js'
 },
 output: {
  filename: '[name].js',
  path:PATH.resolve(__dirname,'_assets/js')
 },
 devtool: ENV ? 'none' : 'inline-source-map',
 module: {
  rules: [
   {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
     loader: 'babel-loader',
    }
   },
   {
    test: /\.(sa|sc|c)ss$/,
    use: [
     {
      loader:MINI_CSS_EXTACT_PLUGIN.loader
     },
     { 
      loader: 'css-loader',
      options: {
       sourceMap: true
      }
     },
     {
      loader:'sass-loader',
      options:{
       sourceMap:true
      }
     }
    ]
   }
  ]
 },
 plugins:plugins,
 optimization: { //JSライブラリ別ファイル出力
  splitChunks: {
   cacheGroups:{
    commons: {
     test: /[\\/]node_modules[\\/]/,
     name: 'vendor',
     chunks: 'initial'
    }
   }
  }
 },
 
};

const presets = [
  [
    "@babel/preset-env",
    {
      targets: { ie:'11' },
      corejs:{ version:3 },
      useBuiltIns: 'usage',
      // debug: true
    }
  ]
];

module.exports = { presets };
